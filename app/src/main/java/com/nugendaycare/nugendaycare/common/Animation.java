package com.nugendaycare.nugendaycare.common;

import android.app.Application;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ViewFlipper;

/**
 * Created by IKON on 3/18/2016.
 */
public class Animation extends Application {
    //  ---------------------------------- Start - Animation ------------------------------------------
    private ViewFlipper mFlpAccount;

    public Animation(ViewFlipper mFlpAccount) {
        this.mFlpAccount = mFlpAccount;
    }

    public  Animation nextAccount (){
        mFlpAccount.setInAnimation(inFromRightAnimation());
        mFlpAccount.setOutAnimation(outToLeftAnimation());
        mFlpAccount.showNext();
        return this;
    }

    public  Animation prevAccount (){
        mFlpAccount.setInAnimation(inFromLeftAnimation());
        mFlpAccount.setOutAnimation(outToRightAnimation());
        mFlpAccount.showPrevious();
        return this;
    }

    public android.view.animation.Animation inFromRightAnimation() {

        android.view.animation.Animation inFromRight = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  +1.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;

    }
    private android.view.animation.Animation outToLeftAnimation() {

        android.view.animation.Animation outtoLeft = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  -1.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        outtoLeft.setDuration(500);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;

    }

    private android.view.animation.Animation inFromLeftAnimation() {

        android.view.animation.Animation inFromLeft = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  -1.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        inFromLeft.setDuration(500);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;

    }
    private android.view.animation.Animation outToRightAnimation() {

        android.view.animation.Animation outtoRight = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  +1.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        outtoRight.setDuration(500);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;

    }

    public android.view.animation.Animation inFromBottomAnimation() {

        android.view.animation.Animation inFromBottom = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  +1.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        inFromBottom.setDuration(500);
        inFromBottom.setInterpolator(new AccelerateInterpolator());
        return inFromBottom;

    }
    private android.view.animation.Animation outToTopAnimation() {

        android.view.animation.Animation outtoTop = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  -1.0f
        );
        outtoTop.setDuration(500);
        outtoTop.setInterpolator(new AccelerateInterpolator());
        return outtoTop;

    }

    private android.view.animation.Animation inFromTopAnimation() {

        android.view.animation.Animation inFromTop = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  -1.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        inFromTop.setDuration(500);
        inFromTop.setInterpolator(new AccelerateInterpolator());
        return inFromTop;

    }
    private android.view.animation.Animation outToBottomAnimation() {

        android.view.animation.Animation outtoBottom = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   +1.0f
        );
        outtoBottom.setDuration(500);
        outtoBottom.setInterpolator(new AccelerateInterpolator());
        return outtoBottom;

    }

}
