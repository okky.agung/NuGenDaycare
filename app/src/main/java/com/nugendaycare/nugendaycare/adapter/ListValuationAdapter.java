package com.nugendaycare.nugendaycare.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nugendaycare.nugendaycare.R;
import com.nugendaycare.nugendaycare.rowItem.ListValuationRowItem;

import java.util.ArrayList;

/**
 * Created by riskisyendi on 3/26/17.
 */

public class ListValuationAdapter extends ArrayAdapter<ListValuationRowItem> {

    Context context;
    int layoutResourceId;
    //    String id;
    ArrayList<ListValuationRowItem> mListValuationArrayList = new ArrayList<ListValuationRowItem>();

    public ListValuationAdapter(Context context, int layoutResourceId, ArrayList<ListValuationRowItem> mListValuationArrayList) {
        super(context, layoutResourceId, mListValuationArrayList);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.mListValuationArrayList = mListValuationArrayList;
    }

    static class UserHolder {
        TextView mTxtSession,mTxtSessionDate;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        UserHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new UserHolder();
            holder.mTxtSession = (TextView) row.findViewById(R.id.txtSession);
            holder.mTxtSessionDate = (TextView) row.findViewById(R.id.txtSessionDate);
            row.setTag(holder);

        } else {
            holder = (UserHolder) row.getTag();
        }

        ListValuationRowItem mListValuationRowItem = mListValuationArrayList.get(position);
        holder.mTxtSession.setText(mListValuationRowItem.getTextSession());
        holder.mTxtSessionDate.setText(mListValuationRowItem.getTextSessionDate());

        return row;

    }
}
