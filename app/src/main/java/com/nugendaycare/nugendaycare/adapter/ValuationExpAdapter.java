package com.nugendaycare.nugendaycare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nugendaycare.nugendaycare.R;
import com.nugendaycare.nugendaycare.rowItem.ValuationChildRowitem;
import com.nugendaycare.nugendaycare.rowItem.ValuationGroupRowItem;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by riskisyendi on 3/21/17.
 */

public class ValuationExpAdapter extends com.nugendaycare.nugendaycare.common.AnimatedExpandableListView.AnimatedExpandableListAdapter {


    //    com.appscapeblog.expandablelistview.AnimatedExpandableListView.AnimatedExpandableListAdapter
    private Context context;
    private ArrayList<ValuationGroupRowItem> headerList; // header titles
    // child data in format of header title, child title
    private HashMap<ValuationGroupRowItem, ArrayList<ValuationChildRowitem>> childList;

    public ValuationExpAdapter(Context context, ArrayList<ValuationGroupRowItem> headerList,
                                      HashMap<ValuationGroupRowItem, ArrayList<ValuationChildRowitem>> childList) {
        this.context = context;
        this.headerList = headerList;
        this.childList = childList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.childList.get(this.headerList.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getRealChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

        final ValuationChildRowitem childText = (ValuationChildRowitem) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_exp_child_detail_valuation, null);
        }

        TextView mTxtSubtitle = (TextView) convertView.findViewById(R.id.txtSubtitle);
        CheckBox mCbxChecklist = (CheckBox) convertView.findViewById(R.id.cbxChecklist);

        mTxtSubtitle.setText(childText.getTextSubtitle());
        mCbxChecklist.setChecked(childText.getSelectedChecklist());

        return convertView;
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        return this.childList.get(this.headerList.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.headerList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.headerList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        ValuationGroupRowItem groupText = (ValuationGroupRowItem) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_exp_group_detail_valuation, null);
        }

        ImageView mImgPanel = (ImageView) convertView.findViewById(R.id.imgPanel);

        if (isExpanded){
            mImgPanel.setImageResource(R.drawable.ic_arrow_drop_up);

        }else {

            mImgPanel.setImageResource(R.drawable.ic_arrow_drop_down);
        }

        TextView mTxtTitle = (TextView) convertView.findViewById(R.id.txtTitle);

        mTxtTitle.setText(groupText.getTextTitle());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
