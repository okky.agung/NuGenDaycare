package com.nugendaycare.nugendaycare.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nugendaycare.nugendaycare.R;
import com.nugendaycare.nugendaycare.common.CircleGraphicsUtil;
import com.nugendaycare.nugendaycare.controller.ProfileChildActivity;
import com.nugendaycare.nugendaycare.controller.ValuationActivity;
import com.nugendaycare.nugendaycare.rowItem.ListChildRowItem;

import java.util.ArrayList;

/**
 * Created by riskisyendi on 3/21/17.
 */

public class ListChildAdapter extends ArrayAdapter<ListChildRowItem> {

    Context context;
    int layoutResourceId;
    CircleGraphicsUtil mCircleGraphicsUtil;
    //    String id;
    ArrayList<ListChildRowItem> mListChildArrayList = new ArrayList<ListChildRowItem>();

    public ListChildAdapter(Context context, int layoutResourceId, ArrayList<ListChildRowItem> mCatArrayList) {
        super(context, layoutResourceId, mCatArrayList);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.mListChildArrayList = mCatArrayList;
    }

    static class UserHolder {
        ImageView mImgPicChild,mBtnEditChild;
        TextView mTxtChildName,mTxtDOB,mTxtAgeChild;
        Button mBtnValuation;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        UserHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new UserHolder();
            holder.mImgPicChild = (ImageView) row.findViewById(R.id.imgPicChild);
            holder.mTxtChildName = (TextView) row.findViewById(R.id.txtChildName);
            holder.mTxtDOB = (TextView) row.findViewById(R.id.txtDOB);
            holder.mTxtAgeChild = (TextView) row.findViewById(R.id.txtAgeChild);
            holder.mBtnEditChild = (ImageView) row.findViewById(R.id.btnEditChild);
            holder.mBtnValuation = (Button) row.findViewById(R.id.btnValuation);
            row.setTag(holder);

        } else {
            holder = (UserHolder) row.getTag();
        }

        ListChildRowItem mListChildRowItem = mListChildArrayList.get(position);
        holder.mImgPicChild.setImageBitmap(mCircleGraphicsUtil.getRoundedShape(decodeFile(context,mListChildRowItem.getImagePicChild() ), 200));
        holder.mTxtChildName.setText(mListChildRowItem.getTextChildName());
        holder.mTxtDOB.setText(mListChildRowItem.getTextDOB());
        holder.mTxtAgeChild.setText(mListChildRowItem.getTextAgeChild());

        holder.mBtnEditChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProfileChildActivity.class);
                context.startActivity(intent);
            }
        });

        holder.mBtnValuation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ValuationActivity.class);
                context.startActivity(intent);
            }
        });

        return row;

    }

    public static Bitmap decodeFile(Context context, int resId) {
        try {
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(context.getResources(), resId, o);
            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 200;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true)
            {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale++;
            }
            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeResource(context.getResources(), resId, o2);
        } catch (Exception e) {
        }
        return null;
    }
}
