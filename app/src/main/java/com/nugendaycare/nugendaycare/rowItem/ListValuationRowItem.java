package com.nugendaycare.nugendaycare.rowItem;

/**
 * Created by riskisyendi on 3/29/17.
 */

public class ListValuationRowItem {

    Integer id;
    String  textSession,textSessionDate;

    public ListValuationRowItem(Integer id, String textSession, String textSessionDate) {
        this.id = id;
        this.textSession = textSession;
        this.textSessionDate = textSessionDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTextSession() {
        return textSession;
    }

    public void setTextSession(String textSession) {
        this.textSession = textSession;
    }

    public String getTextSessionDate() {
        return textSessionDate;
    }

    public void setTextSessionDate(String textSessionDate) {
        this.textSessionDate = textSessionDate;
    }
}
