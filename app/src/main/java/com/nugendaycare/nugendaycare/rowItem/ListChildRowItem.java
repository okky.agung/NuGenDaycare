package com.nugendaycare.nugendaycare.rowItem;

/**
 * Created by riskisyendi on 3/21/17.
 */

public class ListChildRowItem {

    Integer id, imagePicChild;
    String  textChildName,textDOB,textAgeChild;

    public ListChildRowItem(Integer id, Integer imagePicChild, String textChildName, String textDOB, String textAgeChild) {
        this.id = id;
        this.imagePicChild = imagePicChild;
        this.textChildName = textChildName;
        this.textDOB = textDOB;
        this.textAgeChild = textAgeChild;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getImagePicChild() {
        return imagePicChild;
    }

    public void setImagePicChild(Integer imagePicChild) {
        this.imagePicChild = imagePicChild;
    }

    public String getTextChildName() {
        return textChildName;
    }

    public void setTextChildName(String textChildName) {
        this.textChildName = textChildName;
    }

    public String getTextDOB() {
        return textDOB;
    }

    public void setTextDOB(String textDOB) {
        this.textDOB = textDOB;
    }

    public String getTextAgeChild() {
        return textAgeChild;
    }

    public void setTextAgeChild(String textAgeChild) {
        this.textAgeChild = textAgeChild;
    }
}
