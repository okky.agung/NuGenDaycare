package com.nugendaycare.nugendaycare.rowItem;

/**
 * Created by riskisyendi on 3/21/17.
 */

public class ValuationChildRowitem {

    Integer childID;
    String  textSubtitle;
    Boolean selectedChecklist;

    public ValuationChildRowitem(Integer childID, String textSubtitle, Boolean selectedChecklist) {
        this.childID = childID;
        this.textSubtitle = textSubtitle;
        this.selectedChecklist = selectedChecklist;
    }

    public Integer getChildID() {
        return childID;
    }

    public void setChildID(Integer childID) {
        this.childID = childID;
    }

    public String getTextSubtitle() {
        return textSubtitle;
    }

    public void setTextSubtitle(String textSubtitle) {
        this.textSubtitle = textSubtitle;
    }

    public Boolean getSelectedChecklist() {
        return selectedChecklist;
    }

    public void setSelectedChecklist(Boolean selectedChecklist) {
        this.selectedChecklist = selectedChecklist;
    }
}
