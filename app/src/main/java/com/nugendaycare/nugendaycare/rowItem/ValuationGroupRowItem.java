package com.nugendaycare.nugendaycare.rowItem;

import java.util.ArrayList;

/**
 * Created by riskisyendi on 3/21/17.
 */

public class ValuationGroupRowItem {

    Integer groupID;
    String textTitle;
    ArrayList<ValuationChildRowitem> Items;

    public ValuationGroupRowItem(Integer groupID, String textTitle) {
        this.groupID = groupID;
        this.textTitle = textTitle;
    }

    public ValuationGroupRowItem(Integer groupID, String textTitle, ArrayList<ValuationChildRowitem> items) {
        this.groupID = groupID;
        this.textTitle = textTitle;
        Items = items;
    }

    public Integer getGroupID() {
        return groupID;
    }

    public void setGroupID(Integer groupID) {
        this.groupID = groupID;
    }

    public String getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(String textTitle) {
        this.textTitle = textTitle;
    }

    public ArrayList<ValuationChildRowitem> getItems() {
        return Items;
    }

    public void setItems(ArrayList<ValuationChildRowitem> items) {
        Items = items;
    }
}

