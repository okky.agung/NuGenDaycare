package com.nugendaycare.nugendaycare.controller;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nugendaycare.nugendaycare.R;
import com.nugendaycare.nugendaycare.adapter.ListChildAdapter;
import com.nugendaycare.nugendaycare.adapter.ListValuationAdapter;
import com.nugendaycare.nugendaycare.common.CircleGraphicsUtil;
import com.nugendaycare.nugendaycare.rowItem.ListChildRowItem;
import com.nugendaycare.nugendaycare.rowItem.ListValuationRowItem;

import java.util.ArrayList;

public class ValuationActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ImageView mImgPicChild, mImgPicNanny;
    private TextView mTxtChildName, mTxtDOB, mTxtAgeChild, mTxtNannyName, mTxtNannyEmail;
    private Button mBtnGraph, mBtnNannyInfo;
    private ListView mLvListValuation;
    private ListValuationAdapter mListValuationAdapter;
    private ArrayList<ListValuationRowItem> mListValuationArrayList = new ArrayList<ListValuationRowItem>();
    private CircleGraphicsUtil mCircleGraphicsUtil;
    private Bitmap mBitmapChild, mBitmapPicNanny;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valuation);

        toolbar();
        pageValuation();

    }

    public void toolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.valuation);
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.fontPrimary));
        mToolbar.setNavigationIcon(R.drawable.ic_back);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    public void pageValuation(){
        mImgPicChild = (ImageView) findViewById(R.id.imgPicChild);
        mImgPicNanny = (ImageView) findViewById(R.id.imgPicNanny);
        mTxtChildName = (TextView) findViewById(R.id.txtChildName);
        mTxtDOB = (TextView) findViewById(R.id.txtDOB);
        mTxtAgeChild = (TextView) findViewById(R.id.txtAgeChild);
        mTxtNannyName = (TextView) findViewById(R.id.txtNannyName);
        mTxtNannyEmail = (TextView) findViewById(R.id.txtNannyEmail);
        mBtnGraph = (Button) findViewById(R.id.btnGraph);
        mBtnNannyInfo = (Button) findViewById(R.id.btnNannyInfo);
        mLvListValuation = (ListView) findViewById(R.id.lvListValution);

//        ListValuationRowItem(Integer id, String textSession, String textSessionDate)
        mListValuationArrayList.add(new ListValuationRowItem(1,"1","3 March 2017"));
        mListValuationArrayList.add(new ListValuationRowItem(2,"2","10 March 2017"));
        mListValuationArrayList.add(new ListValuationRowItem(3,"3","17 March 2017"));
        mListValuationArrayList.add(new ListValuationRowItem(4,"4","24 March 2017"));
        mListValuationArrayList.add(new ListValuationRowItem(5,"5","30 March 2017"));
        mListValuationArrayList.add(new ListValuationRowItem(6,"6","3 Appril 2017"));

        mListValuationAdapter = new ListValuationAdapter(this, R.layout.list_valuation, mListValuationArrayList);
        mLvListValuation = (ListView) findViewById(R.id.lvListValution);
        mLvListValuation.setItemsCanFocus(false);
        mLvListValuation.setAdapter(mListValuationAdapter);

        mLvListValuation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, final int position, long id) {
                ListValuationRowItem mListValuationRowItem = (ListValuationRowItem) parent.getItemAtPosition(position);
                Intent intent = new Intent(ValuationActivity.this, DetailValuationActivity.class);
                startActivity(intent);
            }
        });

        mCircleGraphicsUtil = new CircleGraphicsUtil();

        mBitmapChild = BitmapFactory.decodeResource(getResources(), R.drawable.josua);
        mImgPicChild.setImageBitmap(mCircleGraphicsUtil.getCircleBitmap(mBitmapChild, 16));
        mTxtChildName.setText("Josua Suherman");
        mTxtDOB.setText("24 April 2010");
        mTxtAgeChild.setText("7");

        mBitmapPicNanny = BitmapFactory.decodeResource(getResources(), R.drawable.chelsea_islan);
        mImgPicNanny.setImageBitmap(mCircleGraphicsUtil.getCircleBitmap(mBitmapPicNanny, 16));
        mTxtNannyName.setText("Chelsea Islan");
        mTxtNannyEmail.setText("chelsea.islan@gmail.com");

        mBtnGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(ValuationActivity.this, "Feature Under Development", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        mBtnNannyInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ValuationActivity.this, DetailNannyActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
