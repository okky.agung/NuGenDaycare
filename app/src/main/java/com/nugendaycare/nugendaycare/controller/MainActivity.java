package com.nugendaycare.nugendaycare.controller;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nugendaycare.nugendaycare.R;
import com.nugendaycare.nugendaycare.adapter.ListChildAdapter;
import com.nugendaycare.nugendaycare.common.CircleGraphicsUtil;
import com.nugendaycare.nugendaycare.rowItem.ListChildRowItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FloatingActionButton mFabRegisterChild;
    private ListView mLvListChild;
    private ListChildAdapter mListChildAdapter;
    private ArrayList<ListChildRowItem> mListChildArrayList = new ArrayList<ListChildRowItem>();
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToogle;
    private NavigationView mNavView;
    private ImageView mImgPicUser;
    private TextView mTxtUserName,mTxtEmail;
    private CircleGraphicsUtil mCircleGraphicsUtil;
    private Bitmap mBitmapPicUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar();
        navigationDrawer();
        pageMain();

    }

    public void toolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    public void navigationDrawer(){
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mToogle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(mToogle);
        mToogle.syncState();

        mNavView = (NavigationView) findViewById(R.id.navView);
        mNavView.setNavigationItemSelectedListener(this);

        View header=mNavView.getHeaderView(0);
        mImgPicUser = (ImageView) header.findViewById(R.id.imgPicUser);
        mTxtUserName = (TextView) header.findViewById(R.id.txtUserName);
        mTxtEmail = (TextView) header.findViewById(R.id.txtEmail);

        mCircleGraphicsUtil = new CircleGraphicsUtil();

        mBitmapPicUser = BitmapFactory.decodeResource(getResources(), R.drawable.raisa);
        mImgPicUser.setImageBitmap(mCircleGraphicsUtil.getCircleBitmap(mBitmapPicUser, 16));
        mTxtEmail.setText("Raisa Andriana");
        mTxtEmail.setText("raisa.andriana@gmail.com");
    }

    public void pageMain(){
        mLvListChild = (ListView) findViewById(R.id.lvListChild);
        mFabRegisterChild = (FloatingActionButton) findViewById(R.id.fabRegisterChild);

//        ListChildRowItem(Integer id, Integer imagePicChild, String textChildName, String textDOB, String textAgeChild)
        mListChildArrayList.add(new ListChildRowItem(1,R.drawable.josua,"Josua","24 April 2010","7"));
        mListChildArrayList.add(new ListChildRowItem(1,R.drawable.sherina,"Sherina","1 January 2008","9"));

        /**
         * set item into adapter
         */
        mListChildAdapter = new ListChildAdapter(this, R.layout.list_child, mListChildArrayList);
        mLvListChild = (ListView) findViewById(R.id.lvListChild);
        mLvListChild.setItemsCanFocus(false);
        mLvListChild.setAdapter(mListChildAdapter);



        mFabRegisterChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,RegisterChildActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            Intent intent = new Intent(MainActivity.this,ProfileActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_logout) {
            Intent intent = new Intent(MainActivity.this,AccountActivity.class);
            startActivity(intent);
            finish();
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
