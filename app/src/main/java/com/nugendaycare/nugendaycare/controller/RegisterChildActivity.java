package com.nugendaycare.nugendaycare.controller;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nugendaycare.nugendaycare.R;
import com.nugendaycare.nugendaycare.common.CircleGraphicsUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class RegisterChildActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ImageView mImgPicChild;
    private EditText mEdtChildName, mEdtDOBChild;
    private Spinner mSpinPOBChild;
    private List<String> mListPOBChild;
    private ArrayAdapter<String> mSpinPOBChildArrayAdapter;
    private Button mBtnRegisterChild;
    private Bitmap mBitmapPicChild;
    private CircleGraphicsUtil mCircleGraphicsUtil;
    private Calendar calendar;
    private int day, month, year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_child);

        toolbar();
        pageRegisterChild();

    }

    public void toolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.register_child);
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.fontPrimary));
        mToolbar.setNavigationIcon(R.drawable.ic_back);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    public void pageRegisterChild(){
        mEdtChildName = (EditText) findViewById(R.id.edtChildName);
        mEdtDOBChild = (EditText) findViewById(R.id.edtDOBChild);
        mSpinPOBChild = (Spinner) findViewById(R.id.spinPOBChild);
        mImgPicChild = (ImageView) findViewById(R.id.imgPicChild);
        mBtnRegisterChild = (Button) findViewById(R.id.btnRegisterChild);

        String[] pobChild = new String[]{
                "Jakarta",
                "Bandung",
                "Padang",
                "Aceh",
                "Makassar"
        };

        mListPOBChild = new ArrayList<>(Arrays.asList(pobChild));

        // Initializing an ArrayAdapter
        mSpinPOBChildArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_pob, mListPOBChild) {
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView spinItem = (TextView) view;
                if (position % 2 == 1) {
                    spinItem.setTextColor(getResources().getColor(R.color.fontPrimary));
                    spinItem.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                } else {
                    spinItem.setTextColor(getResources().getColor(R.color.fontPrimary));
                    spinItem.setBackgroundColor(getResources().getColor(R.color.white));
                }
                return view;
            }
        };

        mSpinPOBChildArrayAdapter.setDropDownViewResource(R.layout.spinner_pob);
        mSpinPOBChild.setAdapter(mSpinPOBChildArrayAdapter);
        mSpinPOBChild.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mEdtDOBChild.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //To show current date in the datepicker
                Calendar mcurrentDate=Calendar.getInstance();
                year=mcurrentDate.get(Calendar.YEAR);
                month=mcurrentDate.get(Calendar.MONTH);
                day=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(RegisterChildActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */

                        int month = selectedmonth + 1;
                        String formattedMonth = "" + month;
                        String formattedDayOfMonth = "" + selectedday;

                        if(month < 10){
                            formattedMonth = "0" + month;
                        }
                        if(selectedday < 10){

                            formattedDayOfMonth = "0" + selectedday;
                        }
                        mEdtDOBChild.setText(selectedyear + "/" + formattedMonth + "/" + formattedDayOfMonth);

                    }
                },year, month, day);
                mDatePicker.setTitle("Select Date of Birth");
                mDatePicker.show();
            }
        });

        mBtnRegisterChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterChildActivity.this,MainActivity.class);
                startActivity(intent);
                Toast toast = Toast.makeText(RegisterChildActivity.this, "Registration your child successfull", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        cameraControl();

    }

    public void cameraControl(){
        // retrieve a reference to the UI button
        mImgPicChild = (ImageView) findViewById(R.id.imgPicChild);
        // handle button clicks
        /**
         * Click method to handle user pressing button to launch camera
         */
        mImgPicChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogImage();
            }
        });
    }

    private void dialogImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        TextView title = new TextView(this);
        title.setText("Add Photo");
        title.setTextColor(ContextCompat.getColor(this, R.color.fontPrimary));
        title.setBackgroundColor(ContextCompat.getColor(this, R.color.bgPrimary));
        title.setPadding(20, 20, 20, 20);
        title.setGravity(Gravity.CENTER);
        title.setTextSize(22);

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterChildActivity.this);

        builder.setCustomTitle(title);

        // builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    //                if (v.getId() == R.id.imgPicUserName) {
                    try {
                        // use standard intent to capture an image
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        /*create instance of File with name img.jpg*/
                        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                        /*put uri as extra in intent object*/
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                        startActivityForResult(intent, 1);
                    } catch (ActivityNotFoundException anfe) {
                        // display an error message
                        String errorMessage = "Whoops - your device doesn't support capturing images!";
                        Toast toast = Toast.makeText(RegisterChildActivity.this, errorMessage, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent();
                    // call android default gallery
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.putExtra("crop", "true");
                    // ******** code for crop image
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    // indicate output X and Y
                    intent.putExtra("outputX", 256);
                    intent.putExtra("outputY", 256);

                    try {
                        intent.putExtra("return-data", true);
                        startActivityForResult(Intent.createChooser(intent,"Complete action using"),3);

                    }catch (Exception E){
                        E.printStackTrace();
                    }
                }else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            // user is returning from capturing an image using the camera
            if (requestCode == 1) {
                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                //Crop the captured image using an other intent
                try {
				/*the user's device may not support cropping*/
                    performCrop(Uri.fromFile(file));
                } catch (ActivityNotFoundException aNFE) {
                    //display an error message if user device doesn't support
                    String errorMessage = "Sorry - your device doesn't support the crop action!";
                    Toast toast = Toast.makeText(RegisterChildActivity.this, errorMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
            if (requestCode == 2) {
                //Create an instance of bundle and get the returned data
                Bundle extras = data.getExtras();
                // get the cropped bitmap
                mBitmapPicChild = extras.getParcelable("data");
                // retrieve a reference to the ImageView
                mImgPicChild = (ImageView) findViewById(R.id.imgPicChild);
                // display the returned cropped image
                mCircleGraphicsUtil = new CircleGraphicsUtil();
                // picView.setImageBitmap(graphicUtil.getRoundedShape(thePic,(float)1.5,92));

                mImgPicChild.setImageBitmap(mCircleGraphicsUtil.getCircleBitmap(mBitmapPicChild, 16));
            }
        }
    }
    /**
     * Helper method to carry out crop operation
     */
    public void performCrop(Uri picUri) {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, 2);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
