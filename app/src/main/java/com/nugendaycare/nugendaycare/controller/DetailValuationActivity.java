package com.nugendaycare.nugendaycare.controller;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.nugendaycare.nugendaycare.R;
import com.nugendaycare.nugendaycare.adapter.ValuationExpAdapter;
import com.nugendaycare.nugendaycare.common.AnimatedExpandableListView;
import com.nugendaycare.nugendaycare.rowItem.ValuationChildRowitem;
import com.nugendaycare.nugendaycare.rowItem.ValuationGroupRowItem;

import java.util.ArrayList;
import java.util.HashMap;

public class DetailValuationActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private AnimatedExpandableListView mLvExpDetailValuation;
    private ValuationExpAdapter mValuationExpAdapter;
    private ArrayList<ValuationGroupRowItem> headerList;
    HashMap<ValuationGroupRowItem, ArrayList<ValuationChildRowitem>> childList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_valuation);

        toolbar();
        pageDetailValuation();

    }

    public void toolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.detail_val);
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.fontPrimary));
        mToolbar.setNavigationIcon(R.drawable.ic_back);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void pageDetailValuation(){
        // get the listview
        mLvExpDetailValuation = (AnimatedExpandableListView) findViewById(R.id.lvExpDetailValuation);

        // preparing list data
        prepareListData();

        mValuationExpAdapter = new ValuationExpAdapter(this, headerList, childList);

        // setting list adapter
        mLvExpDetailValuation.setAdapter(mValuationExpAdapter);

        // Listview Group click listener
        mLvExpDetailValuation.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                // We call collapseGroupWithAnimation(int) and
                // expandGroupWithAnimation(int) to animate group
                // expansion/collapse.
                if (mLvExpDetailValuation.isGroupExpanded(groupPosition)) {
                    mLvExpDetailValuation.collapseGroupWithAnimation(groupPosition);
                    Log.d("MainActivity", "Collapsing");
                } else {
                    mLvExpDetailValuation.expandGroupWithAnimation(groupPosition);
                    Log.d("MainActivity", "Expanding");
                }
                return true;
            }

        });

        // Listview Group expanded listener
        mLvExpDetailValuation.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {


            @Override
            public void onGroupExpand(int groupPosition) {
                    /*Toast.makeText(getApplicationContext(),
                            headerList.get(groupPosition) + " Expanded",
                            Toast.LENGTH_SHORT).show();*/
            }

        });

        //Listview Group collasped listener
        mLvExpDetailValuation.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                    /*Toast.makeText(getApplicationContext(),
                            headerList.get(groupPosition) + " Collapsed",
                            Toast.LENGTH_SHORT).show();*/

            }
        });

        //Listview on child click listener
        mLvExpDetailValuation.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        getApplicationContext(),
                        headerList.get(groupPosition)
                                + " : "
                                + childList.get(
                                headerList.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });

    }

    private void prepareListData() {

        headerList = new ArrayList<ValuationGroupRowItem>();
        childList = new HashMap<ValuationGroupRowItem, ArrayList<ValuationChildRowitem>>();

        // Adding child data

        headerList.add(new ValuationGroupRowItem(1,"Title 1"));
        headerList.add(new ValuationGroupRowItem(2,"Title 2"));
        headerList.add(new ValuationGroupRowItem(3,"Title 3"));
        headerList.add(new ValuationGroupRowItem(4,"Title 4"));
        headerList.add(new ValuationGroupRowItem(5,"Title 5"));

        // Adding child data
        ArrayList<ValuationChildRowitem> title1 = new ArrayList<ValuationChildRowitem>();
        title1.add(new ValuationChildRowitem(1,"Subtitle 1.1",true));
        title1.add(new ValuationChildRowitem(2,"Subtitle 1.2",false));
        title1.add(new ValuationChildRowitem(3,"Subtitle 1.3",true));
        title1.add(new ValuationChildRowitem(4,"Subtitle 1.4",true));

        ArrayList<ValuationChildRowitem> title2 = new ArrayList<ValuationChildRowitem>();
        title2.add(new ValuationChildRowitem(1,"Subtitle 2.1",false));
        title2.add(new ValuationChildRowitem(2,"Subtitle 2.2",false));
        title2.add(new ValuationChildRowitem(3,"Subtitle 2.3",true));
        title2.add(new ValuationChildRowitem(4,"Subtitle 2.4",true));

        ArrayList<ValuationChildRowitem> title3 = new ArrayList<ValuationChildRowitem>();
        title3.add(new ValuationChildRowitem(1,"Subtitle 3.1",true));
        title3.add(new ValuationChildRowitem(2,"Subtitle 3.2",true));
        title3.add(new ValuationChildRowitem(3,"Subtitle 3.3",true));
        title3.add(new ValuationChildRowitem(4,"Subtitle 3.4",true));

        ArrayList<ValuationChildRowitem> title4 = new ArrayList<ValuationChildRowitem>();
        title4.add(new ValuationChildRowitem(1,"Subtitle 4.1",true));
        title4.add(new ValuationChildRowitem(2,"Subtitle 4.2",true));
        title4.add(new ValuationChildRowitem(3,"Subtitle 4.3",true));
        title4.add(new ValuationChildRowitem(4,"Subtitle 4.4",true));

        ArrayList<ValuationChildRowitem> title5 = new ArrayList<ValuationChildRowitem>();
        title5.add(new ValuationChildRowitem(1,"Subtitle 5.1",true));
        title5.add(new ValuationChildRowitem(2,"Subtitle 5.2",true));
        title5.add(new ValuationChildRowitem(3,"Subtitle 5.3",true));
        title5.add(new ValuationChildRowitem(4,"Subtitle 5.4",true));

        // Header, Child data
        childList.put(headerList.get(0), title1);
        childList.put(headerList.get(1), title2);
        childList.put(headerList.get(2), title3);
        childList.put(headerList.get(3), title4);
        childList.put(headerList.get(4), title5);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
