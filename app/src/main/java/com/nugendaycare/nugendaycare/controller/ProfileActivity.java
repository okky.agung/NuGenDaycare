package com.nugendaycare.nugendaycare.controller;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nugendaycare.nugendaycare.R;
import com.nugendaycare.nugendaycare.common.CircleGraphicsUtil;

import java.io.File;

public class ProfileActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ImageView mImgPicUser;
    private EditText mEdtUserName, mEdtEmail, mEdtPass, mEdtAdress;
    private Button mBtnChangePass, mBtnSaveChanges;
    private Bitmap mBitmapPicUser;
    private CircleGraphicsUtil mCircleGraphicsUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        toolbar();
        pageProfile();

    }

    public void toolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.profile);
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.fontPrimary));
        mToolbar.setNavigationIcon(R.drawable.ic_back);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void pageProfile(){
        mEdtUserName = (EditText) findViewById(R.id.edtUserName);
        mEdtEmail = (EditText) findViewById(R.id.edtEmail);
        mEdtPass = (EditText) findViewById(R.id.edtPass);
        mEdtAdress = (EditText) findViewById(R.id.edtAdress);
        mBtnChangePass = (Button) findViewById(R.id.btnChangePass);
        mBtnSaveChanges = (Button) findViewById(R.id.btnSaveChanges);
        mImgPicUser = (ImageView) findViewById(R.id.imgPicUser);

        mBtnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(ProfileActivity.this, "Button Change Pass pressed", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        mBtnSaveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this,MainActivity.class);
                startActivity(intent);
                Toast toast = Toast.makeText(ProfileActivity.this, "Save Changes Successfull", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        cameraControl();

    }

    public void cameraControl(){
        // retrieve a reference to the UI button
        mImgPicUser = (ImageView) findViewById(R.id.imgPicUser);
        // handle button clicks
        /**
         * Click method to handle user pressing button to launch camera
         */
        mImgPicUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogImage();
            }
        });
    }

    private void dialogImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        TextView title = new TextView(this);
        title.setText("Change Photo");
        title.setTextColor(ContextCompat.getColor(this, R.color.fontPrimary));
        title.setBackgroundColor(ContextCompat.getColor(this, R.color.bgPrimary));
        title.setPadding(20, 20, 20, 20);
        title.setGravity(Gravity.CENTER);
        title.setTextSize(22);

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);

        builder.setCustomTitle(title);

        // builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    //                if (v.getId() == R.id.imgPicUserName) {
                    try {
                        // use standard intent to capture an image
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        /*create instance of File with name img.jpg*/
                        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                        /*put uri as extra in intent object*/
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                        startActivityForResult(intent, 1);
                    } catch (ActivityNotFoundException anfe) {
                        // display an error message
                        String errorMessage = "Whoops - your device doesn't support capturing images!";
                        Toast toast = Toast.makeText(ProfileActivity.this, errorMessage, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent();
                    // call android default gallery
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.putExtra("crop", "true");
                    // ******** code for crop image
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    // indicate output X and Y
                    intent.putExtra("outputX", 256);
                    intent.putExtra("outputY", 256);

                    try {
                        intent.putExtra("return-data", true);
                        startActivityForResult(Intent.createChooser(intent,"Complete action using"),3);

                    }catch (Exception E){
                        E.printStackTrace();
                    }
                }else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            // user is returning from capturing an image using the camera
            if (requestCode == 1) {
                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                //Crop the captured image using an other intent
                try {
				/*the user's device may not support cropping*/
                    performCrop(Uri.fromFile(file));
                } catch (ActivityNotFoundException aNFE) {
                    //display an error message if user device doesn't support
                    String errorMessage = "Sorry - your device doesn't support the crop action!";
                    Toast toast = Toast.makeText(ProfileActivity.this, errorMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
            if (requestCode == 2) {
                //Create an instance of bundle and get the returned data
                Bundle extras = data.getExtras();
                // get the cropped bitmap
                mBitmapPicUser = extras.getParcelable("data");
                // retrieve a reference to the ImageView
                mImgPicUser = (ImageView) findViewById(R.id.imgPicUser);
                // display the returned cropped image
                mCircleGraphicsUtil = new CircleGraphicsUtil();
                // picView.setImageBitmap(graphicUtil.getRoundedShape(thePic,(float)1.5,92));

                mImgPicUser.setImageBitmap(mCircleGraphicsUtil.getCircleBitmap(mBitmapPicUser, 16));
            }
        }
    }
    /**
     * Helper method to carry out crop operation
     */
    public void performCrop(Uri picUri) {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, 2);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
