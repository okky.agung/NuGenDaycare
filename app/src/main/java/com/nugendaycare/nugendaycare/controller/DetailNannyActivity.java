package com.nugendaycare.nugendaycare.controller;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nugendaycare.nugendaycare.R;
import com.nugendaycare.nugendaycare.common.CircleGraphicsUtil;

public class DetailNannyActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ImageView mImgNannyPic;
    private RatingBar mRatbarNanny;
    private TextView mTxtNannyName, mTxtNannyEmail,mTxtNannyAddress;
    private EditText mEdtComment;
    private Button mBtnSendReview;
    private CircleGraphicsUtil mCircleGraphicsUtil;
    private Bitmap mBitmapPicNanny;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_nanny);

        toolbar();
        pageDetailNanny();

    }

    public void toolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.nanny_info);
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.fontPrimary));
        mToolbar.setNavigationIcon(R.drawable.ic_back);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    public void pageDetailNanny(){
        mImgNannyPic = (ImageView) findViewById(R.id.imgNannyPic);
        mRatbarNanny = (RatingBar) findViewById(R.id.ratbarNanny);
        mTxtNannyName = (TextView) findViewById(R.id.txtNannyName);
        mTxtNannyEmail = (TextView) findViewById(R.id.txtNannyEmail);
        mTxtNannyAddress = (TextView) findViewById(R.id.txtNannyAddress);
        mEdtComment = (EditText) findViewById(R.id.edtComment);
        mBtnSendReview = (Button) findViewById(R.id.btnSendReview);

        mCircleGraphicsUtil = new CircleGraphicsUtil();

        mBitmapPicNanny = BitmapFactory.decodeResource(getResources(), R.drawable.chelsea_islan);
        mImgNannyPic.setImageBitmap(mCircleGraphicsUtil.getCircleBitmap(mBitmapPicNanny, 16));

        mBtnSendReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailNannyActivity.this,MainActivity.class);
                startActivity(intent);
                Toast toast = Toast.makeText(DetailNannyActivity.this, "Thanks for the Review", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
