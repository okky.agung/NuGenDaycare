package com.nugendaycare.nugendaycare.service.api;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public class APIServiceConfig {

    public interface pusApps {

        @FormUrlEncoded
        @POST("/client/register?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void registerData(
                @Field("name") String name,
                @Field("email") String email,
                @Field("password") String password,
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/client/login?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void loginData(
                @Field("email") String email,
                @Field("password") String password,
                @Field("regid") String regid,
                Callback<Response> callback
        );

        @GET("/client/getuser?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void getuser(
                Callback<Response> callback
        );

        @GET("/forumcategories/getforumcategories?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void getforumList(
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/forumcategories/getforumcategories/threads?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void getForumCategory(
                @Field("_id") String _id,
                @Field("fp") Integer fp,
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/forum/postcomment?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void postContent(
                @Field("_id") String _id,
                @Field("postcontent") String postcontent,
                Callback<Response> callback
        );

        @GET("/forum/getforum?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void getforum(
                @Query("fid") String fid,
                @Query("fp") Integer fp,
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/forum/createforum?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void postForum(
                @Field("forumtitle") String forumtitle,
                @Field("forumcontent") String forumcontent,
                @Field("categoryid") String categoryid,
                Callback<Response> callback
        );

        @GET("/forum/gettrending?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void gettranding(
                Callback<Response> callback
        );

        @GET("/news/getnewslist?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void getNews(
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/news/getnewsdetail?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void getnewsdetail(
                @Field("articleid") String articleid,
                Callback<Response> callback
        );

        @GET("/infocategories/getinfocategories?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void getinfocategories(
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/infocategories/getinfocategories/threads?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void getInfoCategories(
                @Field("fid") String fid,
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/news/searchnews?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void getSearch(
                @Field("keyword") String keyword,
                @Field("page") Integer page,
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/report/postreport?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void postreport(
                @Field("phone") String phone,
                @Field("longitude") String longitude,
                @Field("latitude") String latitude,
                @Field("content") String content,
                @Field("category") String category,
                Callback<Response> callback
        );

        @GET("/report/getreportlist?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void getreportlist(
                Callback<Response> callback
        );

        @GET("/report/getreportdetail?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void getreportdetail(
                @Query("id") String id,
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/report/postreport/comment?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void postreport(
                @Field("postcontent") String postcontent,
                @Field("reportid") String reportid,
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/client/updateuser?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void updateuser(
                @Field("avatar") String avatar,
                @Field("phone") String phone,
                @Field("name") String name,
                @Field("email") String email,
                @Field("password") String password,
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/client/updatepassword?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void updatepassword(
                @Field("oldpassword") String oldpassword,
                @Field("newpassword") String newpassword,
                Callback<Response> callback
        );

        @FormUrlEncoded
        @POST("/client/forgetpassword?ak=jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=")
        void forgotPassword(
                @Field("email") String email,
                Callback<Response> callback
        );
    }
}