package com.nugendaycare.nugendaycare.service.api;

import android.content.Context;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;

public class APIRestService {
//    private static String strURL = "https://puspapps-kudaponi.c9users.io/";
    private static String strURL = "http://139.59.245.121:3031/api/v2.0.0/";
//    private static String strURL = "http://192.168.1.188:3000/";
    private retrofit.RestAdapter restAdapter;
    private APIServiceConfig.pusApps apiService;
//    private SPIsLogin isLogin;
//    String Token;
//    String UserId;
//    String Username;

    public APIRestService(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(1, TimeUnit.MINUTES);
        okHttpClient.setConnectTimeout(1, TimeUnit.MINUTES);
//        Token = getToken(context);
//        UserId = getUserId(context);
//        Username = getUsername(context);
        try {
            restAdapter = new retrofit.RestAdapter.Builder()
                    .setEndpoint(strURL)
                    .setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestFacade request) {
//                            request.addHeader("authorization", Token);
//                            request.addHeader("userid", UserId);
//                            request.addHeader("username", Username);
                            request.addHeader("Accept", "application/json");
                        }
                    })
                    .setClient(new OkClient(okHttpClient))
                    .setLogLevel(RestAdapter.LogLevel.BASIC)
                    .build();
        } catch (RetrofitError E) {
            E.printStackTrace();
        }

        apiService = restAdapter.create(APIServiceConfig.pusApps.class);
    }


    public APIServiceConfig.pusApps getService() {
        return apiService;
    }

//    private String getToken(Context context) {
//        isLogin = new SPIsLogin(context);
//        if (isLogin.isLoggedIn() == true) {
//
//            Token = isLogin.gettoken();
//
//        }else{
//            Token = "";
//        }
//        return Token;
//    }
//
//    private String getUserId(Context context) {
//        isLogin = new SPIsLogin(context);
//        if (isLogin.isLoggedIn() == true) {
//            UserId = isLogin.getuserid();
//        }else{
//            UserId = "";
//        }
//
//        return UserId;
//    }
//
//    private String getUsername(Context context) {
//        isLogin = new SPIsLogin(context);
//        if (isLogin.isLoggedIn() == true) {
//            Username = isLogin.getusername();
//        }else{
//            Username = "";
//        }
//
//        return Username;
//    }

}
